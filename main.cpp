#include <iostream>

using namespace std;

struct Tree
{
    int data;

    Tree *left;
    Tree *right;
};

Tree *leaf, *nodeBaru, *root = NULL, *current;

Tree *insert(Tree *&current, int value)
{

    if (current == NULL)
    {
        current = new Tree;
        current->data = value;
        current->left = NULL;
        current->right = NULL;
    }
    else
    {
        if (value < current->data)
            insert(current->left, value);
        else
            insert(current->right, value);
    }

    return current;
}

void preOrder(Tree *current)
{
    // parent kiri kanan
    if (current != NULL)
    {
        cout << " " << current->data;
        preOrder(current->left);
        preOrder(current->right);
    }
}

void inOrder(Tree *current)
{
    // kiri parent kanan
    if (current != NULL)
    {
        inOrder(current->left);
        cout << " " << current->data;
        inOrder(current->right);
    }
}

void postOrder(Tree *current)
{
    // kiri kanan parent
    if (current != NULL)
    {
        postOrder(current->left);
        postOrder(current->right);
        cout << " " << current->data;
    }
}
/*
Tree *createTree(int data)
{
    nodeBaru = new Tree();
    nodeBaru->data = data;
    nodeBaru->left = NULL;
    nodeBaru->right = NULL;

    nodeBaru = parent;
}

Tree *tambahKiri(int data)
{
    nodeBaru = new Tree();
    parent->right = nodeBaru;
    nodeBaru->data = data;
    nodeBaru->left = NULL;
    nodeBaru->right = NULL;
}

void tambahKanan(node *parent)
{
    parent->right = node;
    node->left = NULL;
    node->right = NULL;
}
*/

int main()
{
    insert(leaf, 5);
    insert(leaf, 3);
    insert(leaf, 10);
    insert(leaf, 2);
    insert(leaf, 4);
    insert(leaf, 9);
    insert(leaf, 11);

    preOrder(leaf);
    cout << endl;
    inOrder(leaf);
    cout << endl;
    postOrder(leaf);
}
